#include"univers.h"

float ht=hauteur_tour; //animation de début de jeu

void gererEspace(unsigned char touche, int x, int y){
    
if(fin_jeu(s,tab_platforme[hauteur_cubes*4])==0){
    if(touche==' '){
        glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
        tab_touches.ESPACE=1;

        s.vitesse_chute=1.75;
        s.direction->c.z+=s.vitesse_chute;
                    
        cam.direction->c.z=s.direction->c.z;
        cam.position->c.z=cam.direction->c.z+5;
                       
        printf("vitesse: %f\n",s.vitesse_chute);
        afficher_vect(*s.direction);
        glutPostRedisplay();


    }
    else if(touche=='a'){
        cam.position->c.x=cam.direction->c.x-10;
    }
}
}

void gererEspaceRelache(unsigned char touche, int x, int y){
    if(touche==' '){
        tab_touches.ESPACE=0;
    }
    else if(touche=='a'){
        cam.position->c.x=cam.direction->c.x+10;
    }
}

void gererClavierRelache(int touche, int x, int y){
    if(touche==GLUT_KEY_DOWN){
        tab_touches.BAS=0;
    }
    else if(touche==GLUT_KEY_UP){
        tab_touches.HAUT=0;
    }
    else if(touche==GLUT_KEY_RIGHT){
        tab_touches.DROIT=0;
    }
    else if(touche==GLUT_KEY_LEFT){
        tab_touches.GAUCHE=0;
    }
}

void gererClavier(int touche, int x, int y){
    int collision=0;
    int i=0;

    glutSetKeyRepeat(GLUT_KEY_REPEAT_ON);


    if(fin_jeu(s,tab_platforme[hauteur_cubes*4])==0){
    if(tab_touches.ESPACE==0){

    if(touche==GLUT_KEY_DOWN){
        tab_touches.BAS=1;
        printf("down\n");
            if(!gerer_collisions_murs(s,'b')){
                while(i<=hauteur_cubes*4){

                    collision=gerer_collisions(s,tab_platforme[i], 'b');
                    if(collision==1){
                    break;
                    }
                    i++;
                }
            }
            else{
                collision=1;
            }
            i=0;
            if(collision==0){

                s.direction->c.x+=1;
                if(cam.position->c.x<70){
                cam.direction->c.x+=1;
                cam.position->c.x+=1;
                cam.position->c.y=cam.direction->c.y;
                }
                
            }
        afficher_vect(*cam.direction);
    }

    if(touche==GLUT_KEY_UP){
        tab_touches.HAUT=1;

        printf("up\n");
            if(!gerer_collisions_murs(s,'h')){
            while(i<=hauteur_cubes*4){
                collision+=gerer_collisions(s,tab_platforme[i],'h');
                i++;            

            }
            }
            else{
                collision=1;
            }
            i=0;
            if(collision==0){     

    
            s.direction->c.x-=1;

            if(s.direction->c.x<60){
                cam.direction->c.x-=1;        
                cam.position->c.x-=1;
                cam.position->c.y=s.direction->c.y;
            } 
            }
        afficher_vect(*cam.direction);
    }

    if(touche==GLUT_KEY_RIGHT){
        tab_touches.DROIT=1;
        printf("right\n");        
            if(!gerer_collisions_murs(s,'d')){    
            while(i<=hauteur_cubes*4){
                collision+=gerer_collisions(s,tab_platforme[i],'d');
                i++;

            }     
            }
            else{
                collision=1;
            }
            i=0;
            if(collision==0){
            s.direction->c.y+=1;
            cam.position->c.y=cam.direction->c.y-5;
            cam.direction->c.y+=1;
            }
        
        afficher_vect(*cam.direction);
    }

    if(touche==GLUT_KEY_LEFT){
        tab_touches.GAUCHE=1;
        printf("left\n");        

            if(!gerer_collisions_murs(s,'g')){
            while(i<=hauteur_cubes*4){
                collision+=gerer_collisions(s,tab_platforme[i],'g');
                i++;
            }       
            }
            else{
                collision=1;
            } 
            i=0;
            if(collision==0){

            s.direction->c.y-=1;
            cam.direction->c.y-=1; 
            cam.position->c.y=cam.direction->c.y+5;
            }
        afficher_vect(*cam.direction);
    }
    }

    glutPostRedisplay();
    }

}

void animer(){
        int i=0,collision;

    //animation du début de jeu
    if(ht>1){ 
        cam.position->c.z=ht+5;
        cam.direction->c.z=ht;
        ht=ht-0.5;
        glutPostRedisplay();

    }
       else{
           if(fin_jeu(s,tab_platforme[hauteur_cubes*4])==0){
            if(gerer_collisions_murs(s,'s')==0){ //si pas de collision mur lors d'un saut
                while(i<=hauteur_cubes*4){//collisions avec les plateformes

                    collision=gerer_collisions(s,tab_platforme[i], 's');
                    
                    if(collision==1){
                        break;
                    }
                    i++;
                }
                if(collision==0){ //chute
                    s.vitesse_chute-=s.v;
                    s.vitesse_sphere=0.5;
                    //saut latéraux
                    if(tab_touches.DROIT==1){
                        if(gerer_collisions_murs(s,'d')==0){    
                            s.direction->c.y+=s.vitesse_sphere;
                            cam.direction->c.y=s.direction->c.y;
                            cam.position->c.y=cam.direction->c.y-5;
                        }
                    }
                    else if(tab_touches.GAUCHE==1){
                        if(gerer_collisions_murs(s,'g')==0){ 
                        s.direction->c.y-=s.vitesse_sphere;
                        cam.direction->c.y=s.direction->c.y;
                        cam.position->c.y=cam.direction->c.y+5;
                        }
                    }
                    else if(tab_touches.HAUT==1){
                        if(gerer_collisions_murs(s,'h')==0){ 
                            s.direction->c.x-=s.vitesse_sphere;
                            if(s.direction->c.x<60){
                            cam.direction->c.x=s.direction->c.x;
                            cam.position->c.x=cam.direction->c.x+10;
                            } 
                        }
                    }
                    else if(tab_touches.BAS==1){
                        if(gerer_collisions_murs(s,'b')==0){ 
                            s.direction->c.x+=s.vitesse_sphere;
                            if(cam.position->c.x<70){
                            cam.direction->c.x=s.direction->c.x;
                            cam.position->c.x=cam.direction->c.x+10;
                            }
                        }
                    }          
                    //chute verticale
                    s.direction->c.z+=s.vitesse_chute;
                    cam.direction->c.z=s.direction->c.z;
                    cam.position->c.z=cam.direction->c.z+5;
                }
                else{ //atterissage sur une plateforme
                    if(s.direction->c.z>tab_platforme[i].lo.z){
                        s.direction->c.z=tab_platforme[i].po.z+2;
                        cam.direction->c.z=s.direction->c.z;
                        cam.position->c.z=cam.direction->c.z+5;                
                    }
                    else{ //chute libre            
                        s.vitesse_chute-=s.v;
                        s.direction->c.z-=s.vitesse_chute;
                        cam.direction->c.z=s.direction->c.z;
                        cam.position->c.z=cam.direction->c.z+5;
                    }

                }
            }
            else{ //collision mur
                s.direction->c.z=1;

                cam.direction->c.z=s.direction->c.z;
                cam.position->c.z=cam.direction->c.z+5;
            }
                

            glutPostRedisplay();
                }
       }
}



void affichage(){

 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    

    glFrustum(-1,1,-1,1,1,500);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(cam.position->c.x,cam.position->c.y,cam.position->c.z,
                cam.direction->c.x,cam.direction->c.y,cam.direction->c.z,0,0,1);

    //draw_repere();
     draw_tour();
     glColor3d(0.34, 0.16, 0);
    afficher_texte(infos,GLUT_BITMAP_TIMES_ROMAN_24);

    int i;

    for(i=0;i<hauteur_cubes*4;i++){
         draw_platforme(tab_platforme[i].lo.x,tab_platforme[i].lo.y,tab_platforme[i].lo.z,
                        tab_platforme[i].po.x,tab_platforme[i].po.y,tab_platforme[i].po.z);       
    }
    draw_platforme_arrivee(tab_platforme[i].lo.x,tab_platforme[i].lo.y,tab_platforme[i].lo.z,
                        tab_platforme[i].po.x,tab_platforme[i].po.y,tab_platforme[i].po.z);
    glTranslatef(s.direction->c.x,s.direction->c.y,s.direction->c.z);
    glColor3f(0.2, 0.9, 0.9);
    draw_sphere();
    glutSwapBuffers();
    }

int main(int argc, char* argv[]){


  
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutInitWindowSize(800,800);
    glutInitWindowPosition(50,50);

    glutCreateWindow("Tour infernale");
    glEnable(GL_DEPTH_TEST);


   init_touches(tab_touches);
    s=init_sphere();
    cam=init_camera(cam);
    chrono=120;
    jeu=1;
    draw_alea_platforme();
    glutDisplayFunc(affichage);
    glutIdleFunc(animer);  
    glutTimerFunc(1000, chronometre, ht);
    glutKeyboardFunc(gererEspace);
    glutKeyboardUpFunc(gererEspaceRelache);
    glutSpecialUpFunc(gererClavierRelache);  
    glutSpecialFunc(gererClavier);

    
     glutMainLoop();
    return 0;

}


