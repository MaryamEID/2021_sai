CC=gcc
OPTIONS= -lglut -lGLU -lGL -lm

main: univers.o sphere.o main.c
	$(CC) -Wall main.c univers.o sphere.o -o main $(OPTIONS)

univers.o: univers.h univers.c
	gcc -c univers.c $(OPTIONS)

sphere.o: sphere.h sphere.c
	gcc -c sphere.c

clean:
	rm  *.o main
