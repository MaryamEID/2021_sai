#include"sphere.h"


vecteur def_vecteur(int x, int y, int z){
    vecteur v;
    v.c.x=x;
    v.c.y=y;
    v.c.z=z;
    return v;
}

sphere init_sphere(){
    sphere s;
    s.direction=malloc(sizeof(vecteur));
    *(s.direction)=def_vecteur(largeur_tour-10,largeur_tour/2,1);
    s.sens=-1;
    s.vitesse_sphere=0;
    s.vitesse_chute=0;
    s.v=0.15;
    return s;
}
void draw_sphere(){
    
    glutSolidSphere(rayon_sphere, 30, 30);

}
void afficher_vect(vecteur v){
    printf("x: %f\n",v.c.x);
    printf("y: %f\n",v.c.y);
    printf("z: %f\n",v.c.z);
}