#include "GL/gl.h"
#include "GL/glut.h"
#include <stdio.h>
#include<stdlib.h>


#define rayon_sphere 2
#define hauteur_tour 115
#define largeur_tour 70


/*definir un type vecteur
vecteur vitessse
vecteur vers le haut
3 vecteurs tjs orthonormé
rotation vecteur

deplacement: calculer la nouvelle position M en partant de la precedente+cte k* vecteur vitesse

gestion des collisons:précalculé le nouveau point M si possible valider déplacement sinon non 
*/



typedef struct coordonne{
  float x;
  float y;
  float z;

}coordonne;


typedef struct vecteur{
coordonne c;

}vecteur;



typedef struct sphere{
float sens;
float vitesse_sphere;
float vitesse_chute;
float v;
vecteur *direction;

}sphere;

sphere s;


//dessine la sphère
void draw_sphere();
//initialise les paramètres de la sphère
sphere init_sphere();
//initialise un vecteur
vecteur def_vecteur(int x, int y, int z);

void afficher_vect(vecteur v);