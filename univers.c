#include"univers.h"


void def_repere(){  
  glFrustum(-800,800,-800,800,1,800);

  gluLookAt(cam.position->c.x,cam.position->c.y,cam.position->c.z,
                cam.direction->c.x,cam.direction->c.y,cam.direction->c.z,
                0,0,1);
}

void draw_repere(){
 glBegin(GL_LINES);      /* On trace les 3 axes dans des couleurs différentes */
 
        glColor3ub(255, 0, 0);    /* Axe X en rouge */
        glVertex3i(0,0, 0);
	glVertex3i(30,0, 0);
  
        glColor3ub(0, 255, 0);    /* Axe Y en vert */
        
        glVertex3i(0, 0, 0);
	glVertex3i(0, 30, 0);
	
	glColor3ub(255, 255, 255);   /* Axe Z en blanc */
        glVertex3i(0, 0, 0);
        glVertex3i(0, 0, 30);
 
glEnd(); /* Fin du tracé */

  
}



void draw_tour(){

  glBegin(GL_QUADS);
  // bas gris clair
  glColor3f(0.1, 0.1, 0.1);
  glVertex3f(0, 0, sol_tour);
  glVertex3f(0, largeur_tour, sol_tour);
  glVertex3f(largeur_tour, largeur_tour, sol_tour);
  glVertex3f(largeur_tour, 0, sol_tour);

    // mur bleu
  glColor3f(0.1, 0.8, 0.8);
  glVertex3f(0, 0, sol_tour);
  glVertex3f(0, largeur_tour, sol_tour);
  glVertex3f(0, largeur_tour, hauteur_tour);
  glVertex3f(0, 0, hauteur_tour);

      // mur rouge
  glColor3f(0.8, 0.1, 0.1);
  glVertex3f(0, largeur_tour, sol_tour);
  glVertex3f(largeur_tour, largeur_tour, sol_tour);
  glVertex3f(largeur_tour, largeur_tour, hauteur_tour);
  glVertex3f(0, largeur_tour, hauteur_tour);


      // mur vert
  glColor3f(0.1, 0.8, 0.1);
  glVertex3f(0, 0, sol_tour);
  glVertex3f(largeur_tour, 0, sol_tour);
  glVertex3f(largeur_tour, 0, hauteur_tour);
  glVertex3f(0, 0, hauteur_tour);

      // mur jaune
  glColor3f(0.8, 0.8, 0.1);
  glVertex3f(largeur_tour, 0, sol_tour);
  glVertex3f(largeur_tour, largeur_tour, sol_tour);
  glVertex3f(largeur_tour, largeur_tour, hauteur_tour);
  glVertex3f(largeur_tour, 0, hauteur_tour);

  // plafond
  glColor3f(0.1, 0.5, 0.8);
  glVertex3f(0, 0, hauteur_tour);
  glVertex3f(0, largeur_tour, hauteur_tour);
  glVertex3f(largeur_tour, largeur_tour, hauteur_tour);
  glVertex3f(largeur_tour, 0, hauteur_tour);

  glEnd(); /* Fin du tracé */

}


platforme draw_platforme(float x1, float y1, float z1, float x2, float y2, float z2){
 
  glBegin(GL_QUADS);
    glColor3f(0.9,0.9,0.9);
    glVertex3f(x1,y1,z1);
    glVertex3f(x1,y2,z1);
    glVertex3f(x2,y2,z1);
    glVertex3f(x2,y1,z1);


    glColor3f(0.7,0.7,0.7);
    glVertex3f(x1,y1,z1);
    glVertex3f(x1,y2,z1);
    glVertex3f(x1,y2,z2);
    glVertex3f(x1,y1,z2); 


    glColor3f(0.9,0.9,0.9);
    glVertex3f(x1,y1,z2);
    glVertex3f(x1,y2,z2);
    glVertex3f(x2,y2,z2);
    glVertex3f(x2,y1,z2); 


    glColor3f(0.6,0.6,0.6);
    glVertex3f(x1,y1,z1);
    glVertex3f(x1,y1,z2);
    glVertex3f(x2,y1,z2);
    glVertex3f(x2,y1,z1); 


    glColor3f(0.6,0.6,0.6);
    glVertex3f(x1,y2,z1);
    glVertex3f(x1,y2,z2);
    glVertex3f(x2,y2,z2);
    glVertex3f(x2,y2,z1);     
    

    glColor3f(0.7,0.7,0.7);
    glVertex3f(x2,y1,z1);
    glVertex3f(x2,y2,z1);
    glVertex3f(x2,y2,z2);
    glVertex3f(x2,y1,z2);   
    glEnd(); /* Fin du tracé */
}

platforme draw_platforme_arrivee(float x1, float y1, float z1, float x2, float y2, float z2){
 
  glBegin(GL_QUADS);
    glColor3f(0.1, 0.8, 0.1);
    glVertex3f(x1,y1,z1);
    glVertex3f(x1,y2,z1);
    glVertex3f(x2,y2,z1);
    glVertex3f(x2,y1,z1);


    glColor3f(0.2, 0.8, 0.2);
    glVertex3f(x1,y1,z1);
    glVertex3f(x1,y2,z1);
    glVertex3f(x1,y2,z2);
    glVertex3f(x1,y1,z2); 


    glColor3f(0.1, 0.9, 0.1);
    glVertex3f(x1,y1,z2);
    glVertex3f(x1,y2,z2);
    glVertex3f(x2,y2,z2);
    glVertex3f(x2,y1,z2); 


    glColor3f(0.1, 0.8, 0.1);
    glVertex3f(x1,y1,z1);
    glVertex3f(x1,y1,z2);
    glVertex3f(x2,y1,z2);
    glVertex3f(x2,y1,z1); 


    glColor3f(0.2, 0.8, 0.2);
    glVertex3f(x1,y2,z1);
    glVertex3f(x1,y2,z2);
    glVertex3f(x2,y2,z2);
    glVertex3f(x2,y2,z1);     
    

    glColor3f(0.1, 0.8, 0.1);
    glVertex3f(x2,y1,z1);
    glVertex3f(x2,y2,z1);
    glVertex3f(x2,y2,z2);
    glVertex3f(x2,y1,z2);   
    glEnd(); /* Fin du tracé */
}

void draw_alea_platforme(){
        float z=0;
        int i;
        int k=0;
        float x,y;
        srand(time(NULL)); 

        for(i=0;i<hauteur_cubes;i++){
            x=rand()%30;
            y=rand()%30; 
            tab_platforme[i].lo.x=x;
            tab_platforme[i].lo.y=y;
            tab_platforme[i].lo.z=z;

            tab_platforme[i].po.x=x+5;
            tab_platforme[i].po.y=y+10;
            tab_platforme[i].po.z=z+2;
            z+=3;
        }

        z=0;
        
        for(i=hauteur_cubes;i<hauteur_cubes*2;i++){
                x=rand()%30;
                y=rand()%(60-30)+30; 
                tab_platforme[i].lo.x=x;
                tab_platforme[i].lo.y=y;
                tab_platforme[i].lo.z=z;

                tab_platforme[i].po.x=x+5;
                tab_platforme[i].po.y=y+10;
                tab_platforme[i].po.z=z+2;
                z+=3;
        //printf("%f, %f, %f\n",x,y,z);
        }
        z=0;    

        for(i=hauteur_cubes*2;i<hauteur_cubes*3;i++){
                x=rand()%(60-30)+30;
                y=rand()%20; 
                tab_platforme[i].lo.x=x;
                tab_platforme[i].lo.y=y;
                tab_platforme[i].lo.z=z;

                tab_platforme[i].po.x=x+5;
                tab_platforme[i].po.y=y+10;
                tab_platforme[i].po.z=z+2;
                z+=3;
       //printf("%f, %f, %f\n",x,y,z);
        }
        z=0;  
        for(i=hauteur_cubes*3;i<hauteur_cubes*4;i++){
                x=rand()%(60-40)+40;
                y=rand()%(60-40)+40; 
                tab_platforme[i].lo.x=x;
                tab_platforme[i].lo.y=y;
                tab_platforme[i].lo.z=z;

                tab_platforme[i].po.x=x+5;
                tab_platforme[i].po.y=y+10;
                tab_platforme[i].po.z=z+2;
                z+=3;
        //printf("%f, %f, %f\n",x,y,z);
        }
        x=rand()%(60-1)+1;
        y=rand()%(60-1)+1; 
        tab_platforme[i].lo.x=x;
        tab_platforme[i].lo.y=y;
        tab_platforme[i].lo.z=z;

        tab_platforme[i].po.x=x+5;
        tab_platforme[i].po.y=y+10;
        tab_platforme[i].po.z=z+2;
        z=0;
        

}

int min(int a, int b){
  if(a<b)
    return a;
  else
    return b;
}

int max(int a, int b){
  if(a>b)
    return a;
  else
    return b;
}

int gerer_collisions_murs(sphere s, char direction){
    int hx, hy, hz, x, y, z;
    double intersection;
        if(tab_touches.ESPACE==0){
          if(direction=='h'){
          x=s.direction->c.x-3;
          y=s.direction->c.y;
          z=s.direction->c.z;
            if(x<0){
              return 1;
            }
          }
          else if(direction=='b'){
          x=s.direction->c.x+4;
          y=s.direction->c.y;
          z=s.direction->c.z;
            if(x>largeur_tour){
              return 1;
            }
          }
          else if(direction=='d'){
          x=s.direction->c.x;
          y=s.direction->c.y+3;
          z=s.direction->c.z;
            if(y>largeur_tour){
              return 1;
            }
          }
          else if(direction=='g'){
          x=s.direction->c.x;
          y=s.direction->c.y-3;
          z=s.direction->c.z;
            if(y<0){
              return 1;
            }
          }
          else{
            int v_c=s.vitesse_chute-s.v;
          x=s.direction->c.x;
          y=s.direction->c.y;
          z=s.direction->c.z+s.vitesse_chute;
          if(z<1){
            return 1;
          }
          }
        }
        else{
         if(direction=='h'){
          x=s.direction->c.x-s.vitesse_sphere-3;
          y=s.direction->c.y;
          z=s.direction->c.z;
            if(x<0){
              return 1;
            }
          }
          else if(direction=='b'){
          x=s.direction->c.x+s.vitesse_sphere+4;
          y=s.direction->c.y;
          z=s.direction->c.z;
            if(x>largeur_tour){
              return 1;
            }
          }
          else if(direction=='d'){
          x=s.direction->c.x;
          y=s.direction->c.y+s.vitesse_sphere+3;
          z=s.direction->c.z;
            if(y>largeur_tour){
              return 1;
            }
          }
          else if(direction=='g'){
          x=s.direction->c.x;
           y=s.direction->c.y-s.vitesse_sphere-3;
          z=s.direction->c.z;
            if(y<0){
              return 1;
            }
          }
          else{
            int v_c=s.vitesse_chute-s.v;
          x=s.direction->c.x;
          y=s.direction->c.y;
          z=s.direction->c.z+s.vitesse_chute;
          if(z<1){
            return 1;
          }
          }


        }


      return 0;
}


int gerer_collisions(sphere s, platforme p, char direction){
    float hx, hy, hz, x, y, z;
    double intersection;

        if(direction=='h'){
        x=s.direction->c.x-1;
        y=s.direction->c.y;
        z=s.direction->c.z;
        }
        else if(direction=='b'){
        x=s.direction->c.x+1;
        y=s.direction->c.y;
        z=s.direction->c.z;
        }
        else if(direction=='d'){
        x=s.direction->c.x;
        y=s.direction->c.y+1;
        z=s.direction->c.z;
        }
        else if(direction=='g'){
        x=s.direction->c.x;
        y=s.direction->c.y-1;
        z=s.direction->c.z;
        }
        else{
          int v_c=s.vitesse_chute-s.v;
          x=s.direction->c.x;
          y=s.direction->c.y;
          z=s.direction->c.z+s.vitesse_chute;
        }

    hx=max(p.lo.x,min(x,p.po.x));
    hy=max(p.lo.y,min(y,p.po.y));
    hz=max(p.lo.z,min(z,p.po.z));

    intersection=pow((hx-x),2)+pow((hy-y),2)+pow((hz-z),2);

    if(intersection<pow(rayon_sphere,2)){
      return 1;
    }
    else 
      return 0;
}

camera init_camera(camera c){
        c.position=malloc(sizeof(vecteur));
         c.direction=malloc(sizeof(vecteur));
        *(c.position)=def_vecteur(s.direction->c.x+10,s.direction->c.y,hauteur_tour+5);
        *(c.direction)=def_vecteur(s.direction->c.x,s.direction->c.y,hauteur_tour);

        return c;
}

void init_touches(touches t){
  t.ESPACE=0;
  t.HAUT=0;
  t.BAS=0;
  t.DROIT=0;
  t.GAUCHE=0;
}

void afficher_texte(char *texte, void *font) {
    int len,i;
	glRasterPos3f(s.direction->c.x,s.direction->c.y-2,s.direction->c.z+7);
	len = (int) strlen(texte);
	for (i = 0; i < len; i++) {
        glutBitmapCharacter(font,texte[i]);
    }
}

void chronometre(int ht){


  sprintf(infos,"Temps restant: %d s",chrono);
        //printf("chrono: %d\n",chrono);
  if(ht<=2)
    chrono--;

  if(jeu==1)
  glutTimerFunc(1000, chronometre, 0);
}

int fin_jeu(sphere s, platforme p){
  if(chrono>0){
    if(gerer_collisions(s,p,'s')==1){
      if(s.direction->c.z-4>p.lo.z){
            strcpy(infos,"You win !"); 
            afficher_texte(infos, GLUT_BITMAP_TIMES_ROMAN_24);
            jeu=0;
                glutPostRedisplay();
                
            return 1;
      }
    }
  }
  else{
    strcpy(infos,"Game over"); 
    afficher_texte(infos, GLUT_BITMAP_TIMES_ROMAN_24);
    jeu=0;
        glutPostRedisplay();
    return 1;
  }

  return 0;
}