#include "sphere.h"
#include <time.h>
#include <math.h>
#include <string.h>
#include<unistd.h>
#include<time.h>
#define sol_tour -1
#define taille_max_tab 500
#define hauteur_cubes 32



typedef struct touches{

int ESPACE;
int HAUT;
int BAS;
int DROIT;
int GAUCHE;
}touches;


typedef struct camera{
vecteur *position;
vecteur *direction;
}camera;


typedef struct platforme{
  //point le plus proche de l'origine
  coordonne lo;
  //point le plus eloigne de l'origine
  coordonne po;
 
}platforme;

camera cam;

touches tab_touches; //touches pressées
int jeu; //jeu en cours
platforme tab_platforme[taille_max_tab];
char infos[30]; //infos à afficher
int chrono;


void def_repere();
void draw_repere();

//Dessine la tour
void draw_tour();

//Dessine une plateforme
platforme draw_platforme(float x1, float y1, float z1, float x2, float y2, float z2);
//Dessine la plateforme d'arrivée
platforme draw_platforme_arrivee(float x1, float y1, float z1, float x2, float y2, float z2);
//Remplissage du tableau de plateformes
void draw_alea_platforme();

int max(int a, int b);
int min(int a, int b);
//Vérifie s'il y a collision avec un mur selon la direction de la sphere
int gerer_collisions_murs(sphere s, char direction);
//Vérifie s'il y a collision avec une plateforme selon la direction de la sphere
int gerer_collisions(sphere s, platforme p, char direction);

camera init_camera(camera c);
void init_touches(touches t);

//Affiche informations du jeu
void afficher_texte(char *texte, void *font);

void chronometre(int ht);

//Vérifie si le jeu est terminé
int fin_jeu(sphere s, platforme p);